
class Topic:
    def __init__(self,topics):
        self.topic={"general":'*'}
        self.topics = topics

    def __setitem__(self,key,value):
        self.topic[key]=value.strip()

    def __getitem__(self,key):
        topic = self.topic[key]
        if topic in self.topics:
            return topic
        return '*'
